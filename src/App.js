import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            todos: [],
            todoToAdd: ""
        };
        // this.handleInputChange = this.handleChange.bind(this);
    }

    handleInputChange = event => {
        const nextTodoToAdd = event.target.value;
        this.setState({
            todoToAdd: nextTodoToAdd
        });
    };

    handleAddTodoButtonClick = event => {
        const nextTodos = this.state.todos.concat(this.state.todoToAdd);
        const nextTodoToAdd = '';
        this.setState({
            todos: nextTodos,
            todoToAdd: nextTodoToAdd
        });
    };

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">TODO List</h1>
                </header>
                <ul>
                    {this.state.todos.map((todo, index) =>
                            <li key={index}>{todo}</li>)}
                </ul>
                <input
                    type="text"
                    placeholder="New TODO"
                    value={this.state.todoToAdd}
                    onChange={this.handleInputChange}
                />
                <button
                    onClick={this.handleAddTodoButtonClick}>
                        Add TODO
                </button>
            </div>
        );
    }
}

export default App;
